﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	#region Fields

	public static GameManager Instance;

	public List<Agent> agents;
	private GridControl gridControl;

	#endregion

	#region UnityCallbacks

	private void Awake() {
		if (Instance != null) {
			Destroy(gameObject);
			return;
		}
		Instance = this;

		agents = new List<Agent>(FindObjectsOfType<Agent>());
		gridControl = GetComponent<GridControl>();

		gridControl.GridInitiated += SetAgentsOnSpawnPoints;
		//        for (int i = 0; i < agents.Count; i++)
		//        {
		//            NewBrain tempBrain = agents[i].GetComponent<NewBrain>();
		//            if (tempBrain != null)
		//            {
		//                tempBrain.SuperPillTaken += OnSuperPillEvent;
		//            }
		//        }
		agents.ForEach(a => a.SuperPillTaken += OnSuperPillTaken);
	}

	private void Start() {
		SetAgentsOnSpawnPoints();
	}

	private void Update() {
		if (Input.GetKey(KeyCode.R)) {
			SceneManager.LoadScene("Grid 1");
		}
	}

	#endregion

	#region Methods

	private void OnSuperPillTaken(Agent superAgent) {
		foreach (var agent in agents) {
			if (agent != superAgent) {
				agent.SetScared();
			}
			else {
				agent.SetSuperPill();
			}
		}
		StartCoroutine(SuperPillTimer());
	}

	private void SetAgentsOnSpawnPoints() {
		for (int i = 0; i < agents.Count; i++) {
			agents[i].spawnPoint = agents[i].transform.position;
			gridControl.spawnPoints[i] = agents[i].transform.position;
		}
	}

	private IEnumerator SuperPillTimer() {
		yield return new WaitForSeconds(5);
		agents.ForEach(a => a.SetNormal());
	}

	#endregion

	public void OnCollectableDestroyed(Collectable collectable) {
		Debug.Log($"NotImplementedException {collectable}");
	}
}