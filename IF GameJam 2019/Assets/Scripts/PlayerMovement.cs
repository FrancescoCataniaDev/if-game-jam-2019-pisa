﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Sirenix.OdinInspector;

public class PlayerMovement : MonoBehaviour
{
    private Agent agent;
    public int playerNumber;

    public SpriteRenderer currentSprite = new SpriteRenderer();
    private Player player;

    private Vector2 target;

    // private Vector2 target;
    private Vector2 currentPoint;
    private bool hasArrivedOnTarget;
    private bool coroutineIsRunning = false;

    private enum Directions
    {
        up,
        down,
        left,
        right
    };

    private Directions mainDirection;
    [SerializeField] private GameObject triggersHolder;
    [ShowInInspector] private Dictionary<Directions, GameObject> triggers = new Dictionary<Directions, GameObject>();
    private Dictionary<Directions, bool> previousTriggersValue = new Dictionary<Directions, bool>();
    private GridControl gridControl;
    private Rigidbody2D rb2D;
    private Vector2 velocity = new Vector2();

    private Vector3 lastPressed = Vector2.right;

    private void Awake()
    {
        agent = GetComponent<Agent>();
        rb2D = GetComponent<Rigidbody2D>();
        gridControl = FindObjectOfType<GridControl>();
        currentPoint = transform.position;
    }


    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            velocity = Vector2.up;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            velocity = Vector2.down;
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            velocity = Vector2.left;
            //currentSprite.flipX = true;
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            velocity = Vector2.right;
            //currentSprite.flipX = false;
        }

        rb2D.MovePosition(rb2D.position + Time.fixedDeltaTime * agent.speed * velocity);
    }


    private bool CheckForObstacles()
    {
        if (!triggers[mainDirection].GetComponent<DirectionTrigger>().hasCorridor)
            return true;

        return false;
    }

    private void SetTriggersState()
    {
        foreach (var trigger in triggers)
        {
            previousTriggersValue[trigger.Key] = trigger.Value.GetComponent<DirectionTrigger>().hasCorridor;
        }
    }

    private Vector2 GetVector2FromDirection(Directions direction)
    {
        switch (direction)
        {
            case Directions.up:
                return Vector2.up;

            case Directions.down:
                return Vector2.down;

            case Directions.left:
                return Vector2.left;

            case Directions.right:
                return Vector2.right;
        }

        return Vector2.zero;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Collectable") || other.collider.CompareTag("SuperPill"))
        {
            if (agent.state == NewBrain.States.Normal || agent.state == NewBrain.States.SuperPill)
            {
                var collectable = other.gameObject.GetComponent<Collectable>();
                agent.score += collectable.scorePoints;
                if (collectable.isSuperPill)
                {
                    agent.SuperPillTake();
                }

                collectable.DestroyCollectable();
            }
        }

        if (other.collider.gameObject.layer == 9)
        {
            switch (agent.state)
            {
                case NewBrain.States.Normal:
                    Die();


                    break;
                case NewBrain.States.Scared:
                    //diventa eaten
                    agent.SetEaten();
                    break;
                case NewBrain.States.SuperPill:
                    //Nonfa niente 
                    break;
                case NewBrain.States.Eaten:
                    break;
                case NewBrain.States.GameOver:
                    //è morto che cazzo succede nulla!
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    private void Die()
    {
        agent.SetEaten();
    }
    private IEnumerator MoveToNextPoint(Vector2 nextPoint)
    {
        float step = agent.speed * Time.deltaTime;
        while ((Vector2) transform.position != nextPoint)
        {
            transform.position = Vector2.MoveTowards(transform.position, nextPoint, step);
            yield return null;
        }

        currentPoint = nextPoint;
        if (currentPoint == target)
        {
            target = gridControl.GetRandomWalkableTile(currentPoint);
            hasArrivedOnTarget = true;
        }

        coroutineIsRunning = false;
    }
}