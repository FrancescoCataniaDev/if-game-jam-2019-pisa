﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent : MonoBehaviour {
	public int speed;
	public int lives = 3;

	public int score = 0;

	// Start is called before the first frame update
	public UiManager uiManager;
	public NewBrain.States state;
	public Vector3 spawnPoint;
	public event Action<Agent> SuperPillTaken;
	public event Action StateChanged;
	public SpriteRenderer mainSprite;
	public Color normalColor;
	public Color scaredColor;

	private void Awake() {
		SetNormal();
	}

	private void Update() {
		uiManager.Hit(lives);
		uiManager.UpdateScore(score);
	}

	public void SetScared() {
		mainSprite.color = scaredColor;
		state = NewBrain.States.Scared;
		StateChanged?.Invoke();
	}

	public void SetGameOver() {
		mainSprite.color = Color.clear;
		transform.position = new Vector3(-100, -100, 0);
	}

	public void SetNormal() {
		mainSprite.color = normalColor;
		state = NewBrain.States.Normal;
		StateChanged?.Invoke();
	}

	public void SetSuperPill() {
		state = NewBrain.States.SuperPill;
		StateChanged?.Invoke();
	}

	public void SuperPillTake() {
		Debug.Log($"{name} SUPERPILL");
		mainSprite.color = normalColor;
		SuperPillTaken?.Invoke(this);
	}

	public void SetEaten() {
		lives--;
		if (lives == 0) {
			SetDead();
		}
		else {
			state = NewBrain.States.Eaten;
			mainSprite.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
			StartCoroutine(StopBeingEaten());
		}
		StateChanged?.Invoke();
	}

	public void SetDead() {
		state = NewBrain.States.GameOver;
	}

	public IEnumerator StopBeingEaten() {
		yield return new WaitForSeconds(5);
		SetNormal();
		mainSprite.color = normalColor;
	}
}