﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public class Collectable : MonoBehaviour {

    public int scorePoints = 0;
    public bool isSuperPill;
    
    private void Awake()
    {
        gameObject.layer = LayerMask.NameToLayer("Collectable");
    }

    private void OnDestroy() {
        GameManager.Instance.OnCollectableDestroyed(this);
    }

    public void DestroyInEditorCollectable() {
        DestroyImmediate(this.gameObject);
    }

    public void DestroyCollectable() {
        Destroy(this.gameObject);
    }


}
