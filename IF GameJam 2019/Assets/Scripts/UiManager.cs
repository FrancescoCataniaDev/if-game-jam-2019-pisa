﻿using Rewired;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UiManager : MonoBehaviour
{
	public GameObject[] lifes;
	public TMP_Text scoreText;

	public void Hit(int health)
	{
		for (int i = 0; i < lifes.Length; i++)
		{
			lifes[i].SetActive(health - 1 >= i);
		}
	}

	public void UpdateScore(int scorePoints)
	{
		scoreText.text = scorePoints.ToString("0000");
	}
}