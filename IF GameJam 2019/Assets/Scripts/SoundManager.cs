﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public enum SoundsName {

    PACMAN_EAT_FRUIT,
    PACMAN_CHOMP,
    PACMAN_DEATH,
    PACMAN_EAT_GHOST,

    FIRST_PHASE,
    SECOND_PHASE

}


public class SoundManager : MonoBehaviour
{

    public static SoundManager Instance;

    [SerializeField, BoxGroup("Audio Settings")]
    private AudioSource audioSource;
    [BoxGroup("Music Clips"), SerializeField]
    private AudioClip firstPhaseOst, secondPhaseOst, menuOst;

    [BoxGroup("Sounds Clips"), SerializeField]
    private AudioClip chompSound, eatFruitSound, eatGhostSound, deathSound;

    private void Awake() {
        if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(this);
        }
    }


    public void PlayMainSoundtrack(SoundsName name, bool loop = true) {

        AudioClip clip = null;

        switch (name) {
            case SoundsName.FIRST_PHASE:
                clip = firstPhaseOst;
                break;
            case SoundsName.SECOND_PHASE:
                clip = secondPhaseOst;
                break;
        }

        audioSource.clip =clip;
        audioSource.loop = loop;
        audioSource.Play();
    }

    [Button(ButtonSizes.Medium, ButtonStyle.Box, Name = "Test SFX")]
    public void TestAudioClip(SoundsName name) {

        audioSource.PlayOneShot(GetSoundEffect(name));  
    }

    public AudioClip GetSoundEffect(SoundsName name) {
        switch (name) {
            case SoundsName.PACMAN_CHOMP:
                return chompSound;
            case SoundsName.PACMAN_EAT_FRUIT:
                return eatFruitSound;
            case SoundsName.PACMAN_EAT_GHOST:
                return eatGhostSound;
            case SoundsName.PACMAN_DEATH:
                return deathSound;
            default:
                return null;
        }
    }


}
