﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

public class GridControl : MonoBehaviour
{
    #region Fields

    [Title("Grid and Walkable tiles")] public List<Vector2> walkableTiles = new List<Vector2>();
    public Vector2[,] matrixGrid;
    public bool[,] matrixWalkable;
    [SerializeField] private Tilemap walkableTilemap;

    [Space(10), Title("SpawnPoints")] public Tilemap spawnPointsTilemap;

    public List<Vector2> spawnPoints;

    [Space(10), Title("Collectable fields and Properties")] [SerializeField]
    private Tilemap pelletesTilemap;

    [SerializeField] private Tilemap pillsTilemap;
    [SerializeField] private GameObject pelletPrefab;
    [SerializeField] private GameObject pillPrefab;
    [SerializeField] private GameObject collectablesHolder;
    public bool hidePillsTilemap = false, hidePelletsTilemap = false, hideWalkableTilemap = false;
    public List<GameObject> collectableTiles = new List<GameObject>();

    public event Action GridInitiated;

    #endregion

    #region UnityCallbacks

    private void Awake()
    {
        InitGrid();

        walkableTilemap.gameObject.SetActive(!hideWalkableTilemap);
        pillsTilemap.gameObject.SetActive(!hidePillsTilemap);
        pelletesTilemap.gameObject.SetActive(!hidePelletsTilemap);
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium, ButtonStyle.Box, Name = "Spawn Collectables")]
    private void ComputeSpawnCollecatbles()
    {
        // assert for prefabs
        Assert.IsNotNull(pelletPrefab, "The pellet prefab is not referenced!");
        Assert.IsNotNull(pelletesTilemap, "The pellets tilemap is not referenced!");

        // assert for prefabs
        Assert.IsNotNull(pillPrefab, "The pellet prefab is not referenced!");
        Assert.IsNotNull(pillsTilemap, "The pellets tilemap is not referenced!");

        // clear collectables
        collectableTiles.Clear();

        // spawn pellets inside grid
        SpawnCollectables(pelletesTilemap, pelletPrefab);
        // spawn power pills inside grid
        SpawnCollectables(pillsTilemap, pillPrefab);
    }

    private void SpawnCollectables(Tilemap tilemap, GameObject prefabToInstantiate)
    {
        pelletesTilemap.CompressBounds();

        BoundsInt bounds = tilemap.cellBounds;
        TileBase[] allTiles = tilemap.GetTilesBlock(bounds);

        for (int x = 0; x < bounds.size.x; x++)
        {
            for (int y = 0; y < bounds.size.y; y++)
            {
                TileBase tile = allTiles[x + y * bounds.size.x];

                // TODO: clean tile on tilemap

                if (tile == null) continue;

                float tileX = x + tilemap.tileAnchor.x - bounds.size.x / 2;
                float tileY = y - tilemap.tileAnchor.y - bounds.size.y / 2;

                GameObject collectable = Instantiate(prefabToInstantiate, collectablesHolder.transform, true);
                collectable.transform.position = new Vector3(tileX, tileY);
                collectableTiles.Add(collectable);
            }
        }
    }

    [Button(ButtonSizes.Medium, ButtonStyle.Box, Name = "Compute Grid")]
    private void InitGrid()
    {
        PrepareWalkableTiles();
        PrepareSpawnPoints();
    }

    private void PrepareWalkableTiles()
    {
        walkableTilemap.CompressBounds();
        walkableTiles = new List<Vector2>();
        BoundsInt bounds = walkableTilemap.cellBounds;
        TileBase[] allTiles = walkableTilemap.GetTilesBlock(bounds);
        matrixGrid = new Vector2[bounds.size.x, bounds.size.y];
        matrixWalkable = new bool[bounds.size.x, bounds.size.y];

        for (int x = 0; x < bounds.size.x; x++)
        {
            for (int y = 0; y < bounds.size.y; y++)
            {
                TileBase tile = allTiles[x + y * bounds.size.x];
                if (tile != null)
                {
                    float tileX = x + walkableTilemap.tileAnchor.x - bounds.size.x / 2;
                    float tileY = y - walkableTilemap.tileAnchor.y - bounds.size.y / 2;
                    walkableTiles.Add(new Vector2(tileX, tileY));

                    matrixGrid[x, y] = new Vector2(tileX, tileY);
                    matrixWalkable[x, y] = true;

//                    GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
//                    obj.transform.position = new Vector3(tileX, tileY);
                }
                else
                    matrixWalkable[x, y] = false;
            }
        }
    }

    private void PrepareSpawnPoints()
    {
        spawnPointsTilemap.CompressBounds();
        spawnPoints = new List<Vector2>();

        BoundsInt bounds = spawnPointsTilemap.cellBounds;
        TileBase[] allTiles = spawnPointsTilemap.GetTilesBlock(bounds);

        for (int x = 0; x < bounds.size.x; x++)
        {
            for (int y = 0; y < bounds.size.y; y++)
            {
                TileBase tile = allTiles[x + y * bounds.size.x];
                if (tile != null)
                {
                    float tileX = x + spawnPointsTilemap.tileAnchor.x - bounds.size.x / 2;
                    float tileY = y - spawnPointsTilemap.tileAnchor.y - bounds.size.y / 2;
                    spawnPoints.Add(new Vector2(tileX, tileY));
                }
            }
        }
    }

    public Vector2 FindInMatrixGrid(Vector2 vector2)
    {
        
        
        int width = walkableTilemap.cellBounds.size.x;
        int height = walkableTilemap.cellBounds.size.y;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (Vector2.SqrMagnitude(matrixGrid[x, y] - vector2) < 0.01f)
                    return new Vector2(x, y);
            }
        }

        return new Vector2(-100,-100);
    }

    public Vector2 FindInWalkableTiles(Vector2 vector2)
    {
        return walkableTiles.Find(match => match == vector2);
    }

    public Vector2 GetRandomWalkableTile(Vector2 currentPoint)
    {
        bool hasFoundNewTile = false;

        while (!hasFoundNewTile)
        {
            int index = Random.Range(0, walkableTiles.Count);
            if (walkableTiles[index] != currentPoint)
                return walkableTiles[index];
        }

        return Vector2.zero;
    }

    #endregion
}