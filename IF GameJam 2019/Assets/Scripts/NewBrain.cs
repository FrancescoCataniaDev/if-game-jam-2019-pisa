﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NesScripts.Controls.PathFind;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class NewBrain : MonoBehaviour
{
    #region Fields

    public enum Controllers
    {
        PLAYER,
        AI
    }

    public Controllers controller;

    public enum States
    {
        Normal,
        Scared,
        SuperPill,
        Eaten,
        GameOver
    }

    //public States state;

    public enum Directions
    {
        up,
        down,
        left,
        right
    };

    public Directions currentDirection;
    [ShowInInspector] private Vector2 currentPoint;

    public DirectionTrigger triggerU;
    public DirectionTrigger triggerD;
    public DirectionTrigger triggerL;
    public DirectionTrigger triggerR;
    private Dictionary<Directions, DirectionTrigger> triggers = new Dictionary<Directions, DirectionTrigger>();
    [ShowInInspector] private Vector2 target;
    private GridControl gridControl;
    [ShowInInspector] private bool hasArrivedOnTarget;
    private bool coroutineIsRunning = false;
    private bool hasOnCrossroad = false;
    private Coroutine movingCoroutine;
    private Agent agent;
    private List<Point> path;

    #endregion

    #region UnityCallbacks

    private void Awake()
    {
        agent = GetComponent<Agent>();
        gridControl = FindObjectOfType<GridControl>();
        hasArrivedOnTarget = false;
        agent.state = States.Normal;
        agent.StateChanged += OnStateChanged;
    }

    private void Start()
    {
        InitBrain();
    }

    private void Update()
    {
        if (hasArrivedOnTarget)
        {
            CheckState();
        }
    }

    public void ForceArrivedOnTarget()
    {
        if (movingCoroutine != null)
        {
            StopCoroutine(movingCoroutine);
        }

        hasArrivedOnTarget = true;
    }

    private void ChangeDestination(Vector2 newDestination)
    {
        if (movingCoroutine != null)
        {
            StopCoroutine(movingCoroutine);
        }

        movingCoroutine = StartCoroutine(Moving(newDestination));
    }

    public void OnStateChanged()
    {
        ForceArrivedOnTarget();
        //TODO add something?
        switch (agent.state)
        {
            case States.Normal:
                break;
            case States.Scared:
                break;
            case States.SuperPill:
                break;
            case States.Eaten:
                agent.SetEaten();
                break;
            case States.GameOver:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Collectable") || other.collider.CompareTag("SuperPill"))
        {
            if (agent.state == NewBrain.States.Normal || agent.state == NewBrain.States.SuperPill)
            {
                var collectable = other.gameObject.GetComponent<Collectable>();
                agent.score += collectable.scorePoints;
                if (collectable.isSuperPill)
                {
                    agent.SuperPillTake();
                }

                collectable.DestroyCollectable();
            }

            if (other.collider.gameObject.layer == 9)
            {
                switch (agent.state)
                {
                    case NewBrain.States.Normal:
                        //Sbatte contro un agent e muore
                        agent.SetEaten();
                        break;
                    case NewBrain.States.Scared:
                        //diventa eaten
                        agent.SetEaten();
                        break;
                    case NewBrain.States.SuperPill:
                        //Nonfa niente 
                        break;
                    case NewBrain.States.Eaten:
                        break;
                    case NewBrain.States.GameOver:
                        //è morto che cazzo succede nulla!
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }

    #endregion

    #region Methods

    private void InitBrain()
    {
        triggers.Add(Directions.down, triggerD);
        triggers.Add(Directions.left, triggerL);
        triggers.Add(Directions.right, triggerR);
        triggers.Add(Directions.up, triggerU);
        currentPoint = transform.position;
        hasArrivedOnTarget = true;
    }

    public void SetController(Controllers brainController)
    {
        controller = brainController;
    }

    private void SetState(States newState)
    {
        agent.state = newState;
    }

    private void CheckState()
    {
        Vector3 nextDirection;
        bool foundNextTarget;
        float radius;
        switch (agent.state)
        {
            case States.Normal:
                if (CheckForCollectables(out nextDirection))
                {
                    FindNewDirection();
                }
                else
                {
                    Debug.Log("Looks around for more pellets");
                    foundNextTarget = false;
                    radius = 5f;
                    while (!foundNextTarget && radius < 100f)
                    {
                        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius, 1 << 10);
                        if (colliders.Length > 0)
                        {
                            foundNextTarget = true;
                            int minDistance = 1000000;
                            Vector2 nextDestination = Vector2.zero;
                            foreach (var nextPossibleTarget in colliders)
                            {
                                FindPath(nextPossibleTarget.gameObject.transform.position);
                                if (path.Count < minDistance && path.Count > 0)
                                {
                                    minDistance = path.Count;
                                    nextDestination =
                                        gridControl.FindInWalkableTiles(gridControl.matrixGrid[path[0].x, path[0].y]);
                                    target = nextDestination;
                                }
                            }

                            ChangeDestination(nextDestination);
                        }

                        radius *= 2;
                    }
                }

                break;

            case States.Scared:
//TODO look for farthest position from superplayer if close to superplayer
                FindPath(agent.spawnPoint);
                ChangeDestination(gridControl.FindInWalkableTiles(gridControl.matrixGrid[path[0].x, path[0].y]));
                break;

            case States.SuperPill:
//TODO look for other agents near
                Debug.Log("look for other agents near");
                foundNextTarget = false;
                radius = 5f;
                while (!foundNextTarget && radius < 100f)
                {
                    Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius, 1 << 9);
                    if (colliders.Length > 0)
                    {
                        foundNextTarget = true;
                        int minDistance = 1000000;
                        Vector2 nextDestination = Vector2.zero;
                        foreach (var nextPossibleTarget in colliders)
                        {
                            FindPath(nextPossibleTarget.gameObject.transform.position);
                            if (path.Count < minDistance && path.Count > 0)
                            {
                                minDistance = path.Count;
                                nextDestination =
                                    gridControl.FindInWalkableTiles(gridControl.matrixGrid[path[0].x, path[0].y]);
                                target = nextDestination;
                            }
                        }

                        ChangeDestination(nextDestination);
                    }

                    radius *= 2;
                }

                break;

            case States.Eaten:
                FindPath(agent.spawnPoint);
                ChangeDestination(gridControl.FindInWalkableTiles(gridControl.matrixGrid[path[0].x, path[0].y]));
                break;

            case States.GameOver:
                FindPath(agent.spawnPoint);
                ChangeDestination(gridControl.FindInWalkableTiles(gridControl.matrixGrid[path[0].x, path[0].y]));
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private bool CheckForCollectables(out Vector3 nextDirection)
    {
        foreach (var trigger in triggers)
        {
            if (trigger.Value.hasCollectable)
            {
                nextDirection = Vector2ToDirection(trigger.Key);
                return true;
            }
        }

        nextDirection = Vector3.zero;
        return false;
    }

    private bool CheckForOtherAgent()
    {
        RaycastHit2D hit =
            Physics2D.Linecast(transform.position, (Vector2) transform.position + Vector2ToDirection(currentDirection),
                1 << 9 | 1 << 11);
        Debug.DrawLine(transform.position, (Vector2) transform.position + Vector2ToDirection(currentDirection) * 2,
            Color.red);
        if (hit.collider.gameObject.layer == 9 && hit.collider.gameObject != gameObject && hit.collider != null)
        {
            Debug.Log("other agent found");
            return true;
        }

        return false;
    }

    private void RunAway()
    {
//        bool isNearAgents = false;
//        float warningDistance = 5f;
//        float radius = 10f;
//        float distance;
//        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius, 1 << 9);
//
//        if (colliders.Length > 0)
//        {
//            foreach (var nearAgent in colliders)
//            {
//                distance = Vector2.Distance(nearAgent.transform.position)
//            }    
//        }
    }

    private void FindNewDirection()
    {
        for (int i = 0; i < triggers.Count; i++)
        {
            int index = Random.Range(0, triggers.Count);
            if (triggers.ElementAt(index).Value.GetComponent<DirectionTrigger>().hasCorridor
                && triggers.ElementAt(index).Value.GetComponent<DirectionTrigger>().hasCollectable)
            {
                currentDirection = triggers.ElementAt(index).Key;
                target = currentPoint + Vector2ToDirection(currentDirection);
                if (movingCoroutine != null)
                {
                    StopCoroutine(movingCoroutine);
                }

                StartCoroutine(Moving(target));
                break;
            }
        }
    }

    private IEnumerator Moving(Vector2 nextPosition)
    {
        target = nextPosition;
        hasArrivedOnTarget = false;
        float speed = agent.speed * Time.deltaTime;
        while ((Vector2) transform.position != nextPosition)
        {
            transform.position = Vector2.MoveTowards(transform.position, nextPosition, speed);
            yield return null;
        }

        currentPoint = transform.position;
        if (Vector2.SqrMagnitude((Vector2) transform.position - nextPosition) < 0.01f)
        {
            hasArrivedOnTarget = true;
        }
    }

    private void FindPath(Vector2 aimedTarget)
    {
        //currentPoint = transform.position;

        NesScripts.Controls.PathFind.Grid grid = new NesScripts.Controls.PathFind.Grid(gridControl.matrixWalkable);

        Point start = new Point((int) gridControl.FindInMatrixGrid(currentPoint).x,
            (int) gridControl.FindInMatrixGrid(currentPoint).y);

        Point end = new Point((int) gridControl.FindInMatrixGrid(aimedTarget).x,
            (int) gridControl.FindInMatrixGrid(aimedTarget).y);

        path = Pathfinding.FindPath(grid, start, end, Pathfinding.DistanceType.Manhattan);
    }

    private Vector2 Vector2ToDirection(Directions direction)
    {
        switch (direction)
        {
            case Directions.up:
                return Vector2.up;

            case Directions.down:
                return Vector2.down;

            case Directions.left:
                return Vector2.left;

            case Directions.right:
                return Vector2.right;
        }

        return Vector2.zero;
    }

    private Directions? DirectionToVector2(Vector2 vector2)
    {
        if (vector2.y > 0)
            return Directions.up;
        if (vector2.y < 0)
            return Directions.down;
        if (vector2.x < 0)
            return Directions.left;
        if (vector2.x > 0)
            return Directions.right;

        return null;
    }

//    private void OnDrawGizmos()
//    {
//        Gizmos.color = Color.blue;
//        Gizmos.DrawSphere(gridControl.FindInWalkableTiles(transform.position), 0.3f);
//        if (path != null && path.Count > 0)
//        {
//            Gizmos.color = Color.green;
//            Gizmos.DrawSphere(gridControl.matrixGrid[path[0].x, path[0].y], 0.5f);
//            for (int i = 1; i < path.Count - 1; i++)
//            {
//                Gizmos.color = Color.gray;
//                Gizmos.DrawSphere(gridControl.matrixGrid[path[i].x, path[i].y], 0.5f);
//            }
//
//            Gizmos.color = Color.red;
//            Gizmos.DrawSphere(gridControl.matrixGrid[path[path.Count - 1].x, path[path.Count - 1].y], 0.5f);
//        }
//    }

    #endregion
}