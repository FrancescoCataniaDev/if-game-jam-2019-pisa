﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class DirectionTrigger : MonoBehaviour {
	#region Fields

	[FormerlySerializedAs("canMove")] public bool hasCorridor;

	[FormerlySerializedAs("thereIsCollectible")]
	public bool hasCollectable;

	#endregion

	#region UnityCallbacks

	private void Awake() {
		hasCorridor = false;
		hasCollectable = false;
	}

	private void OnTriggerStay2D(Collider2D other) {
		OnTriggerEnter2D(other);
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if (other.CompareTag("Wall")) {
			hasCorridor = false;
		}
		else if (other.CompareTag("Collectable")) {
			hasCollectable = true;
		}
	}

	private void OnTriggerExit2D(Collider2D other) {
		if (other.CompareTag("Wall")) {
			hasCorridor = true;
		}
		else if (other.CompareTag("Collectable")) {
			hasCollectable = false;
		}
	}

	#endregion
}