﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
	private AudioSource audioSource;
	public AudioClip intro;
	public string sceneName;
	public TMP_Text info;
	private bool loading;

	// Start is called before the first frame update
	void Awake()
	{
		audioSource = GetComponent<AudioSource>();
	}

	private void Start()
	{
		audioSource.clip = intro;
		audioSource.PlayOneShot(intro, 1);
		//AudioSource.PlayClipAtPoint(intro, transform.position);
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space) && !loading)
		{
			info.text = "Loading";
			StartCoroutine(Loading());
		}
	}

	private IEnumerator Loading()
	{
		loading = true;
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

		// Wait until the asynchronous scene fully loads
		while (!asyncLoad.isDone)
		{
			yield return null;
		}
	}
}