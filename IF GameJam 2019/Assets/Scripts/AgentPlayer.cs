﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Sirenix.OdinInspector;

public class AgentPlayer : Agent {
	public int playerNumber;
	private Player player;
	private Vector2 target;
	private Vector2 currentPoint;
	private bool hasArrivedOnTarget;
	private bool coroutineIsRunning = false;
	[ShowInInspector] private Dictionary<NewBrain.Directions, GameObject> triggers = new Dictionary<NewBrain.Directions, GameObject>();
	private Dictionary<NewBrain.Directions, bool> previousTriggersValue = new Dictionary<NewBrain.Directions, bool>();
	private GridControl gridControl;
	private Rigidbody2D rb2D;

	private void Awake() {
		rb2D = GetComponent<Rigidbody2D>();
		player = ReInput.players.GetPlayer(playerNumber);
		gridControl = FindObjectOfType<GridControl>();
	}

	private void Update() {
		Movement();
	}

	private void Movement() {
		float moveX = 0f, moveY = 0f;

		moveY = player.GetAxis("MoveVertical");
		moveX = player.GetAxis("MoveHorizontal");

		if (moveX > 0) {
			// GetVector2FromDirection(Agent.Directions.right);
		}
		else if (moveX < 0) {
			// GetVector2FromDirection(Agent.Directions.left);
		}
		if (moveY > 0) {
			// GetVector2FromDirection(Agent.Directions.up);
		}
		else if (moveY < 0) {
			// GetVector2FromDirection(Agent.Directions.down);
		}
	}

	private Vector2 GetVector2FromDirection(NewBrain.Directions direction) {
		switch (direction) {
			case NewBrain.Directions.up:
				return Vector2.up;

			case NewBrain.Directions.down:
				return Vector2.down;

			case NewBrain.Directions.left:
				return Vector2.left;

			case NewBrain.Directions.right:
				return Vector2.right;
		}

		return Vector2.zero;
	}

	private IEnumerator MoveToNextPoint(Vector2 nextPoint) {
		float step = speed * Time.deltaTime;
		while ((Vector2)transform.position != nextPoint) {
			transform.position = Vector2.MoveTowards(transform.position, nextPoint, step);
			yield return null;
		}

		currentPoint = nextPoint;
		if (currentPoint == target) {
			target = gridControl.GetRandomWalkableTile(currentPoint);
			hasArrivedOnTarget = true;
		}
		coroutineIsRunning = false;
	}

	private void Death() {
		lives--;
		uiManager.Hit(lives);
	}

	private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Collectable")) {
			uiManager.UpdateScore(other.GetComponent<PelletCollectable>().Points);
			Destroy(other.GetComponent<GameObject>());
		}
		if (other.CompareTag("Ghost")) {
			Death();
			// other.GetComponent<NewBrain>().Death();
		}
	}
}