﻿using System;
using UnityEngine;

public class Teleport : MonoBehaviour {
	public Transform target;

	private void OnDrawGizmos() {
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(transform.position, 0.5f);
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(target.position, 0.5f);
		Gizmos.color = Color.magenta;
		Gizmos.DrawLine(transform.position, target.position);
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if (other.GetComponent<Agent>() != null) {
			other.gameObject.transform.position = target.transform.position;
			var brain = other.gameObject.GetComponent<NewBrain>();
			if (brain != null) {
				brain.ForceArrivedOnTarget();
			}
		}
	}
}